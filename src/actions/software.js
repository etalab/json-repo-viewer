import fetch from "isomorphic-fetch"

import config from "../config"
import {GET_SOFTWARE, LIST_SOFTWARE} from "../constants"
import {checkStatus} from "../fetchers"


if (config.runningOnServer) {
  var {willGetSoftware, willListSoftwareNames} = require("../server/model")
}


export function getSoftware(name) {
  let promise
  if (config.runningOnServer) {
    promise = willGetSoftware(name)
  } else {
    promise = fetch(`/json/${name}`)
      .then(checkStatus)
      .then(response => response.json())
  }
  return {
    type: GET_SOFTWARE,
    promise,
  }
}


export function getSoftwareIfNeeded(name) {
  return (dispatch, getState) => {
    if (shouldFetchSoftware(getState(), name)) {
      return dispatch(getSoftware(name))
    }
  }
}


export function listSoftwareNames() {
  let promise
  if (config.runningOnServer) {
    promise = willListSoftwareNames()
  } else {
    promise = fetch("/json")
      .then(checkStatus)
      .then(response => response.json())
  }
  return {
    type: LIST_SOFTWARE,
    promise,
  }
}


export function listSoftwareNamesIfNeeded() {
  return (dispatch, getState) => {
    if (shouldFetchSoftwareNames(getState())) {
      return dispatch(listSoftwareNames())
    }
  }
}


function shouldFetchSoftware(state, name) {
  const {softwareByName} = state
  return !softwareByName || !softwareByName[name]
}


function shouldFetchSoftwareNames(state) {
  const {softwareNames} = state
  return !softwareNames || !softwareNames.length
}
