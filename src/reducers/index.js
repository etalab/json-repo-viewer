import {combineReducers} from "redux"
import {routeReducer } from "redux-simple-router"

import {softwareByNameReducer, softwareNamesReducer} from "./software"


// Updates the data for different actions.
export default combineReducers({
  routing: routeReducer,
  softwareByName : softwareByNameReducer,
  softwareNames: softwareNamesReducer,
})
