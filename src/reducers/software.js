import {GET_SOFTWARE, LIST_SOFTWARE} from "../constants"


export function softwareByNameReducer(state = {}, action) {
  switch (action.type) {
  case GET_SOFTWARE:
    const software = action.result
    return {
      ...state,
      [software.name]: software,
    }
  default:
    return state
  }
}

export function softwareNamesReducer(state = [], action) {
  switch (action.type) {
  case LIST_SOFTWARE:
    return [...action.result]
  default:
    return state
  }
}
