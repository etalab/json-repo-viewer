import express from "express"
import path from "path"
import favicon from "serve-favicon"

import config from "../config"
import {willGetSoftware, willListSoftwareNames} from "./model"
import handleRender from "./render"


const app = express()
app.set("trust proxy", config.proxy)

app.use(favicon(path.resolve(__dirname, "../../public/favicon.ico")))
app.get("/json", (req, res, next) => willListSoftwareNames()
  .then(softwareNames => res.json(softwareNames))
  .catch(error => next(error)))
app.get("/json/:name", (req, res, next) => willGetSoftware(req.params.name)
  .then(software => res.json(software))
  .catch(error => next(error)))
app.use("/yaml", express.static(path.resolve(__dirname, "../../../open-software-base-yaml/")))
app.use(handleRender)

const host = config.listen.host
const port = config.listen.port || config.port
app.listen(port, host, () => {
  console.log(`Server listening at http://${host}:${port}/`)
})
