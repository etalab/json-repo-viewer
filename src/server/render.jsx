import {renderToStaticMarkup, renderToString} from "react-dom/server"
import {Provider} from "react-redux"
import {match, RouterContext} from "react-router"
import {syncReduxAndRouter} from "redux-simple-router"

import {fetchRouteData} from "../fetchers"
import configureStore from "../store"
import HtmlDocument from "./html-document"
import routes from "../routes"


export default function handleRender(req, res, next) {
  const store = configureStore()
  match({routes, location: req.url}, (error, redirectLocation, renderProps) => {
    if (error) {
      // // TODO Disable in production?
      // next(error)
      res.status(500).send(error.message)
    } else if (redirectLocation) {
      res.redirect(302, redirectLocation.pathname + redirectLocation.search)
    } else if (renderProps === null) {
      res.status(404).send("Not found")
    } else {
      fetchRouteData(renderProps, store)
        .then(() => res.send(renderHtmlDocument(renderProps, store)))
        .catch((error1) => next(error1))
    }
  })
}


function loadWebpackAssets() {
  const webpackAssetsFilePath = "../../webpack-assets.json"
  let webpackAssets
  if (process.env.NODE_ENV === "production") {
    webpackAssets = require(webpackAssetsFilePath)
  } else if (process.env.NODE_ENV === "development") {
    webpackAssets = require(webpackAssetsFilePath)
    // Do not cache webpack stats: the script file would change since
    // hot module replacement is enabled in the development env
    delete require.cache[require.resolve(webpackAssetsFilePath)]
  }
  return webpackAssets
}


function renderHtmlDocument(renderProps, store) {
  const appHtml = renderToString(
    <Provider store={store}>
      <RouterContext {...renderProps} />
    </Provider>
  )
  const webpackAssets = loadWebpackAssets()
  const html = renderToStaticMarkup(
    <HtmlDocument
      appHtml={appHtml}
      appInitialState={store.getState()}
      cssUrls={webpackAssets.main.css}
      jsUrls={webpackAssets.main.js}
    />
  )
  const doctype = "<!DOCTYPE html>"
  return doctype + html
}
