import yaml from "js-yaml"
import fs from "mz/fs"
import path from "path"


export function willGetSoftware(name) {
  return fs.readFile(path.resolve(__dirname, `../../../open-software-base-yaml/${name}.yaml`), "utf-8")
    .then(yaml.safeLoad)
}


export function willListSoftwareNames() {
  return fs.readdir(path.resolve(__dirname, "../../../open-software-base-yaml/"))
    .then(filenames => filenames.reduce(
      (cores, filename) => {
        if (!filename.startsWith(".")) {
          const parsedFilename = path.parse(filename)
          if (parsedFilename.ext === ".yaml") cores.push(parsedFilename.name)
        }
        return cores
      },
      []))
}
