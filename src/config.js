// Load different configurations (for development, production, etc).
const configFilename = process.env.NODE_ENV || "development"
const config = require("../config/" + configFilename).default

config.runningOnServer = typeof window === "undefined" || !window.document //eslint-disable-line no-undef


export default config
