import {Component} from "react"


export default class Software extends Component {
  static breadcrumbName = "Software"
  render() {
    return (
      <section>
        <p>
          Software
        </p>
        {this.props.children}
      </section>
    )
  }
}
