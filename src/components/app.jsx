import {Component, PropTypes} from "react"
import {Link} from "react-router"
import {Container, Flex, Toolbar} from "rebass"

import config from "../config"
import Breadcrumbs from "./breadcrumbs"
import NavItemLink from "./navitemlink"


export default class App extends Component {
  static breadcrumbName = "Home"
  static propTypes = {
    children: PropTypes.node,
  }
  render() {
    const {children, params, routes} = this.props
    return (
      <div>
        <Toolbar color="blue">
          <Flex>
            {config.title}
          </Flex>
          <NavItemLink to="/">Home</NavItemLink>
          <NavItemLink to="/about">About</NavItemLink>
          <NavItemLink to="/software">Software</NavItemLink>
        </Toolbar>
        <Container>
          <Breadcrumbs
            itemClassName={"btn button-narrow"}
            linkClassName={"btn button-narrow"}
            params={params}
            routes={routes}
            wrapperClassName={"mxn1"}
          />
          {children}
        </Container>
      </div>
    )
  }
}
