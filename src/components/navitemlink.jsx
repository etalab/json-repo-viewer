import classNames from "classnames"
import {Component, PropTypes} from "react"
import {Link} from "react-router"


export default class NavItemLink extends Component {
  static propTypes = {
    children: PropTypes.node,
    flush: PropTypes.bool,
  }
  render () {
    const {children, flush} = this.props
    return (
      <Link {...this.props} className={classNames("NavItem btn", {"p0 py1": flush})}>
        {children}
      </Link>
    )
  }
}
