import {Component, PropTypes} from "react"
import {connect} from "react-redux"
import {Link} from "react-router"

import * as softwareActions from "../actions/software"


@connect(
  state => ({softwareNames: state.softwareNames}),
  dispatch => ({
    onListSoftwareNames: () => dispatch(softwareActions.listSoftwareNames()),
    onListSoftwareNamesIfNeeded: () => dispatch(softwareActions.listSoftwareNamesIfNeeded()),
  }),
)
export default class SoftwareIndex extends Component {
  static fetchData = function(renderProps, store) {
    return store.dispatch(softwareActions.listSoftwareNames())
  }
  static propTypes = {
    softwareNames: PropTypes.arrayOf(PropTypes.string).isRequired,
    onListSoftwareNames: PropTypes.func.isRequired,
    onListSoftwareNamesIfNeeded: PropTypes.func.isRequired,
  }
  componentWillMount() {
    this.props.onListSoftwareNamesIfNeeded()
  }
  render() {
    const {softwareNames, onListSoftwareNames} = this.props
    return (
      <section>
        <p>
          Software
        </p>
        <ul>
          {softwareNames.map(name => <li key={name}><Link to={`/software/${name}`}>{name}</Link></li>)}
        </ul>
        <button onClick={() => onListSoftwareNames()}>Reload</button>
        <pre>{JSON.stringify(softwareNames, null, 2)}</pre>
      </section>
    )
  }
}
