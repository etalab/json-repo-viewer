import {Component, PropTypes} from "react"
import {connect} from "react-redux"

import * as softwareActions from "../actions/software"


@connect(
  state => ({softwareByName: state.softwareByName}),
  dispatch => ({
    onGetSoftware: name => dispatch(softwareActions.getSoftware(name)),
    onGetSoftwareIfNeeded: name => dispatch(softwareActions.getSoftwareIfNeeded(name)),
  }),
)
export default class SoftwareItem extends Component {
  static fetchData = function(renderProps, store) {
    return store.dispatch(softwareActions.getSoftware(renderProps.params.name))
  }
  static propTypes = {
    softwareByName: PropTypes.object.isRequired,
    onGetSoftware: PropTypes.func.isRequired,
    onGetSoftwareIfNeeded: PropTypes.func.isRequired,
  }
  componentWillMount() {
    this.props.onGetSoftwareIfNeeded(this.props.params.name)
  }
  render() {
    const {params, softwareByName, onGetSoftware} = this.props
    const software = softwareByName[params.name]
    if (!software) return (
      <p>Loading {params.name}...</p>
    )
    return (
      <section>
        <p>
          {software.name}
        </p>
        <button onClick={() => onGetSoftware(software.name)}>Reload</button>
        <pre>{JSON.stringify(software, null, 2)}</pre>
      </section>
    )
  }
}
