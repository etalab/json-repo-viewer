import {Component} from "react"


export default class NotFound extends Component {
  static breadcrumbName = "Not Found"
  render() {
    return (
      <p>
        Page not found
      </p>
    )
  }
}
