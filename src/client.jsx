import "babel-polyfill"

import ReactDOM from "react-dom"
import {Provider} from "react-redux"
import {browserHistory, Router} from "react-router"
import "rebass/rebass.css"
import {syncReduxAndRouter} from 'redux-simple-router'

import configureStore from "./store"
import routes from "./routes"


const store = configureStore(window.__INITIAL_STATE__)


syncReduxAndRouter(browserHistory, store)

ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory}>
      {routes}
    </Router>
  </Provider>,
  document.getElementById("app-mount-node"),
)
