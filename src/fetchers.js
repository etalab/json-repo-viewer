// Fetch helpers


export function checkStatus(response) {
  if (response.ok) {
    return response
  } else {
    var error = new Error(response.statusText)
    error.response = response
    throw error
  }
}


// Fetchers


// Fetch data needed to render the view corresponding to a given route.
export function fetchRouteData(renderProps, store) {
  const {components} = renderProps
  const promises = components.reduce((memo, component) => {
    const fetchData = component.WrappedComponent ? // Is it a Redux "connected" component?
      component.WrappedComponent.fetchData :
      component.fetchData
    if (fetchData) {
      memo.push(fetchData(renderProps, store))
    }
    return memo
  }, [])
  return Promise.all(promises)
}
