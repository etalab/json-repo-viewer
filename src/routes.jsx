import {IndexRoute, Route} from "react-router"

import About from "./components/about"
import App from "./components/app"
import Home from "./components/home"
import NotFound from "./components/not-found"
import Software from "./components/software"
import SoftwareIndex from "./components/software-index"
import SoftwareItem from "./components/software-item"


export default (
  <Route component={App} path="/">
    <IndexRoute component={Home} />
    <Route component={About} path="about" />
    <Route component={Software} path="software">
      <IndexRoute component={SoftwareIndex} />
      <Route component={SoftwareItem} path=":name" />
    </Route>
    <Route component={NotFound} isNotFound path="*" />
  </Route>
)
// <Route component={Software} path="software/:urlPath" />
