var ExtractTextPlugin = require("extract-text-webpack-plugin")
var path = require("path")
var webpack = require("webpack")

var writeAssets = require("./src/server/write-assets")


var assetsPath = path.join(__dirname, "public")


module.exports = {
  devtool: "source-map",
  entry: {
    "main": "./src/client.jsx",
  },
  output: {
    path: assetsPath,
    filename: "[name]-[hash].js",
    publicPath: "/",
  },
  module: {
    loaders: [
      {
        loader: ExtractTextPlugin.extract("css-loader!cssnext-loader"),
        test: /\.css$/,
      },
      {
        exclude: /node_modules/,
        loaders: ["babel?stage=0"],
        test: /\.(js|jsx)$/,
      },
      {
        loader: "json-loader",
        test: /\.json$/,
      },
    ],
  },
  node: {
    fs: "empty",
  },
  resolve: {
    extensions: ["", ".js", ".jsx"],
  },
  progress: true,
  plugins: [

    // set global vars
    new webpack.DefinePlugin({
      "process.env": {
        BROWSER: JSON.stringify(true),
        NODE_ENV: JSON.stringify("production"), // clean up some react stuff
      },
    }),

    new webpack.ProvidePlugin({
      React: "react", // For babel JSX transformation which generates React.createElement.
    }),

    // optimizations
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
      },
    }),

    new ExtractTextPlugin("rebass-[contenthash].css"),

    function() { this.plugin("done", writeAssets(path.resolve(__dirname, "webpack-assets.json"))) },
  ],
}
