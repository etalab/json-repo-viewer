# JSON-Repo-Viewer

A HTML viewer for Git repositories of JSON & YAML files

## Installation

For the first time only:

```bash
npm install
```

### Launch the web server

```bash
npm start
```

Open http://localhost:3003/ in your browser.

## Production build

```bash
npm run clean
npm run build
```

Serve the contents of the `public` dir.
For example using [http-server](https://www.npmjs.com/package/http-server)):

```
http-server public
```

Open http://localhost:8080/ in your browser.
