export default {
  host: "localhost",
  listen: {
    host: null,  // Listen to every IPv4 addresses.
    port: null,  // Listen to config.port by default
  },
  port: 3003,
  proxy: false,  // Is this application used behind a trusted proxy?
  title: "JSON-Repo-Viewer",
}
